5x12 in lego
===============

  ![M60 split 5x12](pics/5x12/m60.jpg)

an ortholinear keyboard set in lego 5x12 split with optional encoder and leds. this is a preonic inspired.

this is part of a bigger family of ortholinear keyboards in lego see for reference
https://alin.elena.space/blog/keeblego/

current status and more

https://gitlab.com/m-lego/m65

kicad symbols/footprints are in the m65 repo above

status: all ok

* [x] gerbers designed
* [x] firmware
* [x] breadboard tested
* [x] gerbers printed
* [x] board tested

Features:

* 5x12
* 1 encoder, optional
* led strip, optional
* oled, optional, rev2 only
* 5 pins
* stm32f401 or 411 from we act https://github.com/WeActTC/MiniSTM32F4x1 rev1 or rp2040 rpico compatible pinouts rev 2
* firmware qmk
* uf2 support by default

the pcb
------
- rev 1

* printed pcb

  ![M60 5x12 pcb](pics/5x12/m60-pcb.jpg)

* kicad

  ![M60 5x12 pcb](pics/5x12/m60-pcb.png)


- rev 2 3d render

  ![M60 5x12 pcb](pics/5x12/5x12_rev2_3d.png)


parts
----

* 1 STM32F401 we act pins
* 60 signal diodes 1N4148 , do 35
* 1 encoders
* 2x 350Ω or 220Ω - these are for leds so you may have to compute the R to match your colours and desired brightness.
* 2 leds
* 1 40 pin DIL/DIP sockets whatever you prefer
* led strip 3pins, optional
* 5 pin MX switches 60
* lego 16x32 plate for bottom, and bricks as you please


repo
----

gerbers and kicad files in here  gitlab repo https://gitlab.com/m-lego/m60/

firmware
--------

- rev 1 or simply use this [firmware](firmaware/mlego_m60_rev1_default.uf2) be sure tiny uf2 has the uf2 bootloader.
to install it use https://github.com/adafruit/tinyuf2

```bash
   git clone --recurse-submodules -b mlego https://github.com/alinelena/qmk_firmware.git qmk-alin
   cd qmk-alin
   qmk compile -kb mlego/m60/rev1 -km default

   # or the old way
   make mlego/m60/rev1:default
```

- rev 2 (rpico version) or simply use [firmware](firmaware/mlego_m60_rev2_default.uf2)

```
   git clone --recurse-submodules -b mlego https://github.com/alinelena/qmk_firmware.git qmk-alin
   cd qmk-alin
   qmk compile -kb mlego/m60/rev2 -km default

   # or the old way
   make mlego/m60/rev2:default
```

other pics
----------

  ![M60 5x12](pics/5x12/m60_a.jpg)


